package wanderlust.logica;

public class Aportacio {

	private double diners;
	private boolean esViatgeConcret;
	private Viatge viatge;
	
	public Aportacio(double diners, boolean esViatge){
		
		this.diners=diners;
		this.esViatgeConcret = esViatge;
		
		if(!esViatge)
			viatge = null;
	}
}
