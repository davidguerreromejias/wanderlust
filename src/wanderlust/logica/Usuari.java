package wanderlust.logica;

import wanderlust.modelo.LlistaAportacions;

public class Usuari {

	private int idUser;
	private String nom;
	private LlistaAportacions aportacions;
	
	public Usuari (String nom, int id){
		this.nom = nom;
		this.idUser = id;
	}
	
	public LlistaAportacions getAportacions(){
		return aportacions;
	}
	
	public String getNom(){
		return nom;
	}
	
	public int getID(){
		return idUser;
	}
}
