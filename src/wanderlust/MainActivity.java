package wanderlust;

import wanderlust.activities.AportacioActivity;
import wanderlust.activities.NouViatgeActivity;
import wanderlust.activities.VeureLlistaActivity;

import com.example.wanderlust.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener{

	private static final String TAG = MainActivity.class.getSimpleName() + ".java";
	
	private Button botoViatge, botoAportacio, botoLlista;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		botoViatge = (Button) findViewById(R.id.btnNouViatge);
		botoViatge.setOnClickListener(this);
		
		botoAportacio = (Button) findViewById(R.id.btnNovaAportacio);
		botoAportacio.setOnClickListener(this);
		
		botoLlista = (Button) findViewById(R.id.btnVeureLlista);
		botoLlista.setOnClickListener(this);
		

	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// COMENTARI DE PROVA
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = null;
		Class<?> clase = null;
		if (v == botoViatge){
			clase = NouViatgeActivity.class;
		}else if(v == botoAportacio){
			clase = AportacioActivity.class;
		}else if (v == botoLlista){
			clase = VeureLlistaActivity.class;
		}else{
			Log.d(TAG, "No s'ha trobat l'Activitat");
		}
		intent = new Intent(this, clase);
		startActivity(intent);
	}
}
