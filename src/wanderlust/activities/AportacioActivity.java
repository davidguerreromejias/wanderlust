package wanderlust.activities;

import com.example.wanderlust.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class AportacioActivity extends Activity {

	protected static final String TAG = AportacioActivity.class.getSimpleName() + ".java";
	//VIEW
	private RadioGroup RG;
	private EditText ET;
	
	private int valSelected;
	private OnCheckedChangeListener mCheckedListener = new OnCheckedChangeListener() {

		@Override
		public void onCheckedChanged(RadioGroup group, int checkedId) {
			
			switch (checkedId) {

			case R.id.si:
				valSelected = 1;
				ET.setVisibility(View.VISIBLE);
				Log.d(TAG, "Has apretat S�");
				break;

			case R.id.no:
				valSelected = 0;
				ET.setVisibility(View.INVISIBLE);
				Log.d(TAG, "Has apretat NO");
				break;
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aportacio);
		
		//TODO: No ser� un ET, sin� que desplegar� els viatges actuals
		ET = (EditText) findViewById(R.id.viatge);
		ET.setVisibility(View.INVISIBLE);
		
		LinearLayout ll = (LinearLayout) findViewById(R.id.RG);
		RG = (RadioGroup) ((LinearLayout) ll.findViewById(R.id.ll)).findViewById(R.id.RG);
		RG.setOnCheckedChangeListener(mCheckedListener);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.aportacio, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
